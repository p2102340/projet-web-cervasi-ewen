<?php 
   session_start(); 
   
   @$email=$_POST["email"]; 
   @$password=$_POST["pass"];
   @$valider=$_POST["valider"]; 
   @$erreur=""; 
   if(isset($valider)){ 
      include("connexionBD.php"); 
      $sel=$pdo->prepare("select * from users where email=? limit 1"); 
      $sel->execute(array($email)); 
      $tab=$sel->fetch();
      if($tab){ 
        if(password_verify($password,$tab["password"])){
          //$_SESSION["user"] = $tab;
          $_SESSION["nameLastname"]=ucfirst(strtolower($tab["name"])). 
          " ".strtoupper($tab["lastname"]);
          $_SESSION["autoriser"]="oui";
          header("location:index.php");
          exit;
        }
        else
          $erreur="Mauvais mot de passe !";
      } 
      else 
         $erreur="Mauvais email !"; 
   } 
?> 
<!DOCTYPE html> 
<html> 
   <head> 
      <meta charset="utf-8" /> 
      <style> 
         *{ 
            font-family:arial; 
         } 
         body{ 
            margin:20px; 
         } 
         input{ 
            border:solid 1px #2222AA; 
            margin-bottom:10px; 
            padding:16px; 
            outline:none; 
            border-radius:6px; 
         } 
         .erreur{ 
            color:#CC0000; 
            margin-bottom:10px; 
         } 
         a{ 
            font-size:12pt; 
            color:#EE6600; 
            text-decoration:none; 
            font-weight:normal; 
         } 
         a:hover{ 
            text-decoration:underline; 
         } 
      </style> 
   </head> 
   <body onLoad="document.fo.login.focus()"> 
      <h1>Authentification [ <a href="inscriptionBD.php">Créer un compte</a> ]</h1> 
      <div class="erreur"><?php echo $erreur ?></div> 
      <form name="fo" method="post" action=""> 
         <input type="text" name="email" placeholder="Email" /><br /> 
         <input type="password" name="pass" placeholder="Mot de passe" /><br />
         <input type="submit" name="valider" value="S'authentifier" /> 
      </form> 
   </body> 
</html> 