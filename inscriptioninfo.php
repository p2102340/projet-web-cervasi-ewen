<!doctype html>
<html lang="en">
<head>
    <?php include 'modules/head.php'; ?>
  </head>
  <body class="bg-light">
    <header>
    <?php include 'modules/navbar.php'; ?>
    </br>
    <h2><?php echo $bienvenue?></h2>
    </header>
    <div class="container">
    <div class="jumbotron p-4 p-md-5 text-white rounded bg-dark bg-5">
      <div class="col-md-6 px-0">
        <h1 class="display-4 font-italic">Vos informations</h1>
      </div>
    </div>
  </div>
  <main>
    <?php
    if ( isset( $_POST['firstName'] ) ) {
        $prenom = $_POST['firstName']; 
        if ( isset( $_POST['lastName'] ) ) {
          $nom = $_POST['lastName'];
        }
        if ( isset( $_POST['username'] ) ) {
          $email = $_POST['username'];
        }
        if ( isset( $_POST['hobbie-ski'] ) ) {
          $ski = $_POST['hobbie-ski'];
        }
        if ( isset( $_POST['hobbie-roller'] ) ) {
          $roller = $_POST['hobbie-roller'];
        }
        if ( isset( $_POST['hobbie-foot'] ) ) {
          $foot = $_POST['hobbie-foot'];
        }
        if ( isset( $_POST['hobbie-basket'] ) ) {
          $basket = $_POST['hobbie-basket'];
        }
        if ( isset( $_POST['avatar'] ) ) {
          $avatar = $_POST['avatar'];
        }
        // afficher le résultat
        // echo '<h3>Informations récupérées</h3>'; 
        // echo 'Nom : ' . $nom . ' Prenom : ' . $prenom . ' Email : ' . $email; 
    }
    ?>
    <table>

      <tr> <td>Nom</td> <td><?php if ( isset( $_POST['lastName'] ) ) { echo $nom; }  else{ echo""; }?></td> </tr>

      <tr> <td>Prenom</td> <td><?php if ( isset( $_POST['firstName'] ) ) { echo $prenom; }  else{ echo""; }?></td> </tr>

      <tr> <td>E-mail</td> <td><?php if ( isset( $_POST['username'] ) ) { echo $email; }  else{ echo""; }?></td> </tr>

      <tr> <td>Ski</td> <td><?php if ( isset( $_POST['hobbie-ski'] ) ) { echo "Oui"; }  else{ echo""; }?></td> </tr>

      <tr> <td>Rollers</td> <td><?php if ( isset( $_POST['hobbie-roller'] ) ) { echo "Oui"; }  else{ echo""; }?></td> </tr>

      <tr> <td>Foot</td> <td><?php if ( isset( $_POST['hobbie-foot'] ) ) { echo "Oui"; }  else{ echo""; }?></td> </tr>

      <tr> <td>Basket</td> <td><?php if ( isset( $_POST['hobbie-basket'] ) ) { echo "Oui"; }  else{ echo""; }?></td> </tr>

      <tr> <td>Avatar</td> <td><?php if ( isset( $_POST['avatar'] ) ) { echo $avatar; }  else{ echo""; }?></td> </tr>

    </table>
  </main>

<?php include 'modules/footer.php'; ?>
</body>
</html>

