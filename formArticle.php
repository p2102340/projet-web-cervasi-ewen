<?php include_once 'session.php'; ?>
<?php
   @$message=$_POST["message"]; 
   @$firstname=$_POST["firstName"]; 
   @$contenu=$_POST["contenu"];
   @$photo=$_POST["photo"];
   @$valider=$_POST["valider"]; 
   $erreur=""; 
   if(isset($valider)){ 
      if(empty($firstname)) $erreur="Nom de l'article laissé vide!"; 
      elseif(empty($message)) $erreur="Description de l'article laissé vide!"; 
      elseif(empty($contenu)) $erreur="Contenu de l'article laissé vide!";
      else{ 
         include("connexionBD.php"); 
         $sele=$pdo->prepare("select * from articles where nom=? limit 1"); 
         $sele->execute(array($firstname)); 
         $tab=$sele->fetchAll(); 
         if(count($tab)>0) 
            $erreur="Ce nom d'article existe déjà!"; 
         else{ 
            $ins=$pdo->prepare("insert into articles(nom,description,contenu,photo) values(?,?,?,?)"); 
            if($ins->execute(array($firstname,$message,$contenu,$photo))) 
               header("location:index.php");
         }    
      } 
   } 
?> 
<!doctype html>
<html lang="en">
  <head>
    <?php include 'modules/head.php'; ?>
  </head>
  <body class="bg-light">
    <header>
    <?php include 'modules/navbar.php'; ?>
    </header>
    <div class="container">
    <div class="jumbotron p-4 p-md-5 text-white rounded bg-dark bg-6">
      <div class="col-md-6 px-0">
        <h1 class="display-4 font-italic">Ajoutez un article sur notre site</h1>
        <p class="lead my-3">Lorem, ipsum dolor sit, amet consectetur adipisicing elit. Excepturi itaque autem ducimus dolores ab consectetur, unde distinctio sed nulla modi! Maxime labore debitis quam illo omnis non animi obcaecati molestiae.</p>
      </div>
    </div>
  </div>
    <main>
      <div class="container">
        <div class="row">
          <aside class="col-md-4 order-md-2 mb-4 blog-sidebar">
            <div class="p-4 mb-3 bg-light rounded">
              <h4 class="font-italic">Contact-us</h4>
              <p class="mb-0">
                  <ul>
                    <li>Tel : 000000</li>
                    <li>Address : 12 rue de la république</li>
                    <li>01000 BOURG EN BRESSE</li>
                  </ul>
              </p>
            </div>
          </aside><!-- /.blog-sidebar -->
          <div class="col-md-8 order-md-1">
            <h4 class="mb-3">Formulaire d'ajout d'article</h4>
            <form id="test" method="post">
                <h2><?php echo $erreur ?></h2>
              <fieldset>
                <legend>A propos de l'article</legend>
                <div class="row">
                  <div class="col-md-6 mb-3">
                    <label for="firstName">Titre</label>
                    <input name="firstName" type="text" class="form-control" id="titrearticle">
                  </div>
                </div>

                <div class="mb-3">
                    <label for="content">Description courte (255 charactères max)</label>
                    <textarea name="message" class="form-control" id="content" cols="30" rows="4" maxlength="255"></textarea>
                </div>
              </fieldset>

              <fieldset>
                <legend>Contenu de l'article</legend>
                <div class="mb-3">
                    <textarea name="contenu" class="form-control" id="content" cols="30" rows="20"></textarea>
                </div>
              </fieldset>
              
              <fieldset>
                <legend>Photo de l'article</legend>
                <div class="mb-3">
                <label for="avatar">Choisissez une photo</label>
                <input name="photo" type="file" class="form-control">
              </div>
              </fieldset>
              <button class="btn btn-primary btn-lg btn-block" type="submit" name="valider">Ajout</button>
            </form>
          </div>
        </div>
        
      </div>

    </main>
    
<?php include 'modules/footer.php'; ?>
  
</body>
</html>
