<?php 
   session_start(); 
   @$lastname=$_POST["lastname"]; 
   @$firstname=$_POST["firstname"]; 
   @$email=$_POST["email"]; 
   @$password=$_POST["pass"]; 
   @$repass=$_POST["repass"]; 
   @$valider=$_POST["valider"]; 
   $erreur=""; 
   if(isset($valider)){ 
      if(empty($lastname)) $erreur="Nom laissé vide!"; 
      elseif(empty($firstname)) $erreur="Prénom laissé vide!"; 
      elseif(empty($email)) $erreur="email laissé vide!";
      elseif(empty($password)) $erreur="Mot de passe laissé vide!"; 
      elseif($password!=$repass) $erreur="Mots de passe non identiques!"; 
      else{ 
         include("connexionBD.php"); 
         $sel=$pdo->prepare("select id from users where email=? limit 1"); 
         $sel->execute(array($email)); 
         $tab=$sel->fetchAll(); 
         if(count($tab)>0) 
            $erreur="email existe déjà!"; 
         else{ 
            $ins=$pdo->prepare("insert into users(lastname,firstname,email,password) values(?,?,?,?)"); 
            if($ins->execute(array($lastname,$firstname,$email,md5($password)))) 
               header("location:login.php"); 
         }    
      } 
   } 
?> 
<!DOCTYPE html> 
<html> 
   <head> 
      <meta charset="utf-8" /> 
      <style> 
         *{ 
            font-family:arial; 
         } 
         body{ 
            margin:20px; 
         } 
         input{ 
            border:solid 1px #2222AA; 
            margin-bottom:10px; 
            padding:16px; 
            outline:none; 
            border-radius:6px; 
         } 
         .erreur{ 
            color:#CC0000; 
            margin-bottom:10px; 
         } 
      </style> 
   </head> 
   <body> 
      <h1>Inscription</h1> 
      <div class="erreur"><?php echo $erreur ?></div> 
      <form name="fo" method="post" action=""> 
         <input type="text" name="lastname" placeholder="Lastname" value="<?php echo $lastname?>" /><br /> 
         <input type="text" name="firstname" placeholder="Firstname" value="<?php echo $firstname?>" /><br />
         <input type="text" name="email" placeholder="Login" value="<?php echo $email?>" /><br /> 
         <input type="password" name="pass" placeholder="Mot de passe" /><br /> 
         <input type="password" name="repass" placeholder="Confirmer Mot de passe" /><br /> 
         <input type="submit" name="valider" value="S'authentifier" /> 
      </form> 
   </body> 
</html> 