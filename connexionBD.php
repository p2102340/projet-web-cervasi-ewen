<?php 
    $dsn = 'sqlite:database.db';

    try{
        $pdo=new PDO($dsn);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //echo 'Connexion réussie';
        $_SESSION["autoriser"]="non";
    }catch(PDOException $e){
        echo 'Connexion échouée : ' . $e->getMessage();
    }

    
